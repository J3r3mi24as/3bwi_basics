package at.jeremias.car;

public class Car {
	private String color;
	private double speed;
	private double BasePrice;
	private double BaseConsumption;
	private Producer Producer;
	private Engine Engine;
	private double Price;
	private int Kilometer;
	public Car(String color, double speed, double basePrice, double baseConsumption, Producer producer, Engine engine, int kilometer) {
		super();
		this.color = color;
		this.speed = speed;
		this.BasePrice = basePrice;
		this.BaseConsumption = baseConsumption;
		this.Producer = producer;
		this.Engine = engine;
		this.Kilometer = kilometer;
	}
	public double getPrice() {
		double discount = Producer.getDiscount();
		double DiscountPrice = discount * BasePrice;
		return DiscountPrice;
	}
	public String getType() {
		Boolean EngineTypeBool = Engine.isGas();
		String EngineType;
		if(EngineTypeBool == false)
		{
			EngineType = "Benzin";
		}
		else {
			EngineType = "Diesel";
		};
		
		return EngineType;
		
		
	}
	public double getUsage() {
		double usage;
		if(Kilometer <= 50000)
		{
			usage = BaseConsumption;
		}
		else {
			usage = BaseConsumption * 1.098;
		};
		return usage;	
	}
	
}
