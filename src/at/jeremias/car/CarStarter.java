package at.jeremias.car;

public class CarStarter {
public static void main(String[] args) {
	Producer p1 = new Producer("Rafael","Austria", 0.95);
	Engine e1 = new Engine(false, 45);
	Car c1 = new Car("Blue", 270, 32000, 5, p1, e1, 50000);
	 Person pe1 = new Person("Jeremias","Rusch", "10.6.2");
	// pe1.calculateAge("2018/09/26","2018/08/26");
	 System.out.println(c1.getUsage());
	 System.out.println(c1.getType());
	 System.out.println(c1.getPrice());
}

}
