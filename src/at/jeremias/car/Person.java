package at.jeremias.car;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Period;
import java.util.Date;

public class Person {
	private String fName;
	private String lName;
	private String birthday;
	public Person(String fName, String lName, String birthday) {
		super();
		this.fName = fName;
		this.lName = lName;
		this.birthday = birthday;
	}

	public int calculateAge(
			  LocalDate birthDate,
			  LocalDate currentDate) {
			    // validate inputs ...
		System.out.println(Period.between(birthDate, currentDate).getYears());
			    return Period.between(birthDate, currentDate).getYears();
			}
}
