package at.jeremias.remote;

public class Battery {
	private int batteryStatus;
	private String type;
	
	public Battery(int batteryStatus, String type) {
		super();
		
		if(batteryStatus > 100) {
			this.batteryStatus = 100;
		}
		this.batteryStatus = batteryStatus;
		this.type = type;
	}
	
	public int getBatteryStatus() {
		return batteryStatus;
	}
	
	public void setBatteryStatus(int batteryStatus) {
		this.batteryStatus = batteryStatus;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	
}
